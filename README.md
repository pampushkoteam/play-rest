# play-java-ebean-example

##Тестовое задание

###Play framework back-end developer
Предполагается, что соискатель имеет опыт работы с Play framework 2.x на Java. 

Умеет составлять сложные SQL-запросы для реляционных баз данных.
 
Использовал ранее любую ORM.
 
Имеет представление о формате JSON.
 
И понимает принципы организации асинхронного взаимодействия посредством API.

####Предполагаемый фронт работ
Написание кода контроллеров для обработки входящих JSON/XML-запросов из внешних систем. 

Большинство контроллеров будут иметь схожую логику:
- Извлечение данных из входящего запроса;
- Валидация данных извлеченных из запроса;
- Формирование и выполнение запросов к базе данных;
- Дополнительные действия над извлеченными данными и/или данными полученными из базы данных;
- Формирование ответа на запрос.
- Создание внутренних сервисов выполняющих (по команде или по расписанию) действия над данными системы и/или выполняющих и обрабатывающих запросы к внешним системам;
- Создание или доработка сущностей модели данных.
#### Применяемые технологии
Используется язык Java 8;

Система реализуется на базе Play framework 2.6.x;

В качестве базы данных используем PostgreSQL 9.x;

Для работы с базой данных используется Ebean ORM. 

При этом необходимо уметь писать сложные именованные JPA-запросы, так как возможны ситуации когда ORM не позволит реализовать требуемую логику запроса или же производительность такого сгенерированного запроса будет не оптимальна;

Для работы с JSON используется встроенная в Play framework библиотека Jackson;

Для выполнения удаленных запросов используется встроенная в Play framework библиотека Async Http Client.


####Тестовое задание
На основе примера использования Ebean в Play framework реализовать web-сервис для библиотеки.

Реализовать модель данных следующего вида:

Book:

У книги может быть только один автор (в реальности все сложнее, но для упрощения задачи пусть будет так). Книга может относится к нескольким жанрам.

```
id: UUID
whenCreated — дата/время создания записи
whenUpdated — дата/время последнего изменения записи
version — счетчик кол-ва изменений записи
state: State — состояние записи
name: String
author: Author
year — год публикации
edition — издание
genreCollection: Set<Genre>

Genre:
Жанр может относиться к нескольким книгам.
id: UUID
whenCreated — дата/время создания записи
whenUpdated — дата/время последнего изменения записи
version — счетчик кол-ва изменений записи
state: State — состояние записи
name: String
bookCollection: Set<Book>

Author:
Автор может написать несколько книг.
id: UUID
whenCreated — дата/время создания записи
whenUpdated — дата/время последнего изменения записи
version — счетчик кол-ва изменений записи
state: State — состояние записи
lastName
firstName
middleName
bookCollection: Set<Book>
```
В текущей реализации state может принимать следующие значения: active и deleted.

Обратите внимание, что многие поля в сущностях повторяются, так что необходимо использовать общие базовые классы (например, StatefulModel и/или NamedModel).

Реализовать следующие контроллеры:

##### BookController:

Реализовать метод “add” для добавления новой книги.
Запрос:
```
POST /v1/books
Тело запроса:
{
	"name": "Alice's Adventures in Wonderland",
	"author": {
		"id": "bdd6741d-71b9-493c-9c31-38af19b9e27c"
	},
	"year": 1865,
	"edition": 1,
	"genreCollection": [
{
			"id": "f022820c-cb84-4981-9184-46b6f9a17de8"
		}
	]
}
```
Тело ответа:
```
{
	"id": "0ae2a56c-5dd4-47ed-b230-690c11a786be",
	"whenCreated": "iso8601"
}

GenreController:
Реализовать метод “loadAssociatedBookCollection” для отображения перечня книг выбранного жанра.
Запрос:
GET /v1/genres/f022820c-cb84-4981-9184-46b6f9a17de8/books?offset=0&max=50
Тело ответа:
[{
	"id": "0ae2a56c-5dd4-47ed-b230-690c11a786be",
	"name": "Alice's Adventures in Wonderland",
	"author": {
		"id": "bdd6741d-71b9-493c-9c31-38af19b9e27c",
		"lastName": "Carroll",
		"firstName": "Lewis"
	},
	"year": 1865,
	"edition": 1
}]
```
#####AuthorController:
Реализовать метод update для редактирования автора.

Запрос:
```
PUT /v1/authors/bdd6741d-71b9-493c-9c31-38af19b9e27c
Тело запроса:
	{
	"lastName": "Dodgson",
	"firstName": "Charles",
	"middleName": "Lutwidge"
}
```
Тело ответа:
```
{
	"id": "bdd6741d-71b9-493c-9c31-38af19b9e27ce",
	"whenUpdated": "iso8601",
	"version": 2
}
```

Это задание выполнено на основании исходного исходного кода проекта-примера. Проект расположен здесь:

[https://github.com/playframework/play-java-ebean-example](https://github.com/playframework/play-java-ebean-example)

##Используемые технологии:
- Play

[https://playframework.com/documentation/latest/Home](https://playframework.com/documentation/latest/Home)

- EBean


[https://www.playframework.com/documentation/latest/JavaEbean](https://www.playframework.com/documentation/latest/JavaEbean)
