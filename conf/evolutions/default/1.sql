# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table author (
  id                            uuid not null,
  state                         varchar(7),
  lastname                      varchar(255),
  firstname                     varchar(255),
  middlename                    varchar(255),
  version                       integer not null,
  when_created                  timestamptz not null,
  when_updated                  timestamptz not null,
  constraint ck_author_state check ( state in ('ACTIVE','DELETED')),
  constraint pk_author primary key (id)
);

create table book (
  id                            uuid not null,
  state                         varchar(7),
  name                          varchar(255),
  author_id                     uuid,
  year                          integer,
  edition                       integer,
  version                       integer not null,
  when_created                  timestamptz not null,
  when_updated                  timestamptz not null,
  constraint ck_book_state check ( state in ('ACTIVE','DELETED')),
  constraint pk_book primary key (id)
);

create table book_genre (
  book_id                       uuid not null,
  genre_id                      uuid not null,
  constraint pk_book_genre primary key (book_id,genre_id)
);

create table company (
  id                            bigserial not null,
  name                          varchar(255),
  constraint pk_company primary key (id)
);

create table computer (
  id                            bigserial not null,
  name                          varchar(255),
  introduced                    timestamptz,
  discontinued                  timestamptz,
  company_id                    bigint,
  constraint pk_computer primary key (id)
);

create table genre (
  id                            uuid not null,
  state                         varchar(7),
  name                          varchar(255),
  version                       integer not null,
  when_created                  timestamptz not null,
  when_updated                  timestamptz not null,
  constraint ck_genre_state check ( state in ('ACTIVE','DELETED')),
  constraint pk_genre primary key (id)
);

alter table book add constraint fk_book_author_id foreign key (author_id) references author (id) on delete restrict on update restrict;
create index ix_book_author_id on book (author_id);

alter table book_genre add constraint fk_book_genre_book foreign key (book_id) references book (id) on delete restrict on update restrict;
create index ix_book_genre_book on book_genre (book_id);

alter table book_genre add constraint fk_book_genre_genre foreign key (genre_id) references genre (id) on delete restrict on update restrict;
create index ix_book_genre_genre on book_genre (genre_id);

alter table computer add constraint fk_computer_company_id foreign key (company_id) references company (id) on delete restrict on update restrict;
create index ix_computer_company_id on computer (company_id);


# --- !Downs

alter table if exists book drop constraint if exists fk_book_author_id;
drop index if exists ix_book_author_id;

alter table if exists book_genre drop constraint if exists fk_book_genre_book;
drop index if exists ix_book_genre_book;

alter table if exists book_genre drop constraint if exists fk_book_genre_genre;
drop index if exists ix_book_genre_genre;

alter table if exists computer drop constraint if exists fk_computer_company_id;
drop index if exists ix_computer_company_id;

drop table if exists author cascade;

drop table if exists book cascade;

drop table if exists book_genre cascade;

drop table if exists company cascade;

drop table if exists computer cascade;

drop table if exists genre cascade;

