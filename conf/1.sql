# --- First database schema

# --- !Ups

CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE TYPE STATE AS ENUM ('ACTIVE', 'DELETED');

CREATE TABLE genre (
  --id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  id              VARCHAR(36) DEFAULT gen_random_uuid() NOT NULL,
  when_Created    timestamptz   DEFAULT CURRENT_TIMESTAMP,
  when_Updated    timestamptz,
  version         BIGINT,
  state           STATE       DEFAULT 'ACTIVE',

  name            VARCHAR,

  book_Collection VARCHAR,

  CONSTRAINT pk_genre PRIMARY KEY (id)
);


CREATE TABLE author (
  --id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  id               VARCHAR(36) DEFAULT gen_random_uuid() NOT NULL,
  when_Created     timestamptz   DEFAULT CURRENT_TIMESTAMP,
  when_Updated     timestamptz,
  version          BIGINT,
  state            STATE       DEFAULT 'ACTIVE',

  lastName         VARCHAR,
  firstname        VARCHAR,
  middleName       VARCHAR,

  CONSTRAINT pk_author PRIMARY KEY (id),
  genre_Collection VARCHAR
);


CREATE TABLE book_data (
  --id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  id               VARCHAR(36) DEFAULT gen_random_uuid() NOT NULL,
  when_Created     timestamptz   DEFAULT CURRENT_TIMESTAMP,
  when_Updated     timestamptz,
  version          BIGINT,
  state            STATE       DEFAULT 'ACTIVE',
  name             VARCHAR,
  author           VARCHAR,
  year             INTEGER,
  edition          VARCHAR,
  author_id        VARCHAR(36),
  genre_Collection VARCHAR,

  CONSTRAINT pk_bookdata PRIMARY KEY (id)
);


CREATE TABLE genre_book (
  book_id  VARCHAR(36),
  genre_id VARCHAR(36),

  PRIMARY KEY (book_id, genre_id),
  FOREIGN KEY (book_id) REFERENCES book_data(id),
  FOREIGN KEY (genre_id) REFERENCES genre(id)
);

CREATE TABLE company (
  id   BIGINT NOT NULL,
  name VARCHAR(255),
  CONSTRAINT pk_company PRIMARY KEY (id)
);

CREATE TABLE computer (
  id           BIGINT NOT NULL,
  name         VARCHAR(255),
  introduced   timestamptz,
  discontinued timestamptz,
  company_id   BIGINT,
  CONSTRAINT pk_computer PRIMARY KEY (id)
);

CREATE SEQUENCE company_seq START WITH 1000;

CREATE SEQUENCE computer_seq START WITH 1000;

CREATE SEQUENCE book_data_seq START WITH 1000;

CREATE SEQUENCE author_seq START WITH 1000;

CREATE SEQUENCE genre_seq START WITH 1000;

ALTER TABLE computer
  ADD CONSTRAINT fk_computer_company_1 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
CREATE INDEX ix_computer_company_1
  ON computer (company_id);

ALTER TABLE book_data
  ADD CONSTRAINT fk_bookdata_author_1 FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
CREATE INDEX ix_bookdata_author_1
  ON book_data (author_id);

# --- !Downs

-- SET REFERENTIAL_INTEGRITY FALSE;

DROP TABLE computer;
DROP TABLE company;
DROP TABLE book_data;
DROP TABLE author;
DROP TABLE genre;

DROP SEQUENCE company_seq;
DROP SEQUENCE computer_seq;
DROP SEQUENCE book_data_seq;
DROP SEQUENCE author_seq;
DROP SEQUENCE genre_seq;

DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

GRANT ALL ON SCHEMA public TO PUBLIC;