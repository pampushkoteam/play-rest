# --- Sample dataset

# --- !Ups

-- INSERT INTO book_data (id, name) VALUES (gen_random_uuid(), 'name1');
-- INSERT INTO book_data (id, name) VALUES (gen_random_uuid(), 'name2');
-- INSERT INTO book_data (id, name) VALUES (gen_random_uuid(), 'name3');
-- INSERT INTO book_data (id, name) VALUES (gen_random_uuid(), 'name4');
-- INSERT INTO book_data (id, name) VALUES (gen_random_uuid(), 'name5');
-- INSERT INTO book_data (id, name) VALUES (gen_random_uuid(), 'name6');

--
-- INSERT INTO author (id, firstname, lastName) VALUES (gen_random_uuid(), 'Alexander', 'Petrov');
-- INSERT INTO author (id, firstname, lastName) VALUES (gen_random_uuid(), 'Ivan', 'Ivanov')
-- INSERT INTO author (id, firstname, lastName) VALUES (gen_random_uuid(), 'Petr', 'Zaytsev');
-- INSERT INTO author (id, firstname, lastName) VALUES (gen_random_uuid(), 'Mikhail', 'Maltsev');
-- INSERT INTO author (id, firstname, lastName) VALUES (gen_random_uuid(), 'Natalia', 'Medvedeva');
-- INSERT INTO author (id, firstname, lastName) VALUES (gen_random_uuid(), 'Timur', 'Alirzaev');
--
--
-- INSERT INTO genre (id, name) VALUES (gen_random_uuid(), 'жанр номер 1');
-- INSERT INTO genre (id, name) VALUES (gen_random_uuid(), 'жанр номер 2');
-- INSERT INTO genre (id, name) VALUES (gen_random_uuid(), 'жанр номер 3');
-- INSERT INTO genre (id, name) VALUES (gen_random_uuid(), 'жанр номер 4');


insert into company (id,name) values (  1,'Apple Inc.');
insert into company (id,name) values (  2,'Thinking Machines');
insert into company (id,name) values (  3,'RCA');
insert into company (id,name) values (  4,'Netronics');
insert into company (id,name) values (  5,'Tandy Corporation');


insert into computer (id,name,introduced,discontinued,company_id) values (  1,'MacBook Pro 15.4 inch',null,null,1);
insert into computer (id,name,introduced,discontinued,company_id) values (  2,'CM-2a',null,null,2);
insert into computer (id,name,introduced,discontinued,company_id) values (  3,'CM-200',null,null,2);
insert into computer (id,name,introduced,discontinued,company_id) values (  4,'CM-5e',null,null,2);
insert into computer (id,name,introduced,discontinued,company_id) values (  5,'CM-5','1991-01-01',null,2);
insert into computer (id,name,introduced,discontinued,company_id) values (  6,'MacBook Pro','2006-01-10',null,1);

# --- !Downs

delete from computer;
delete from company;
DELETE FROM book_data;
DELETE FROM genre;
DELETE FROM author