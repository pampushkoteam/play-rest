package repository;

import models.Book;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public interface BookRepository
{
	CompletionStage<Stream<Book>> list();
	
	CompletionStage<Optional<String>> delete(String id);
	
	CompletionStage<Book> create(Book book);
	
	CompletionStage<Optional<Book>> get(String id);
	
	CompletionStage<Optional<Book>> update(String id, Book book);
}

