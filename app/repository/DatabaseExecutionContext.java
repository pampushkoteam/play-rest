package repository;

import akka.actor.ActorSystem;
import play.Logger;
import play.libs.concurrent.CustomExecutionContext;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;

/**
 * Custom execution context, so that blocking database operations don't
 * happen on the rendering thread pool.
 *
 * @link https://www.playframework.com/documentation/latest/ThreadPools
 */
public class DatabaseExecutionContext extends CustomExecutionContext
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	@Inject
	public DatabaseExecutionContext(ActorSystem actorSystem)
	{
		super(actorSystem, "database.dispatcher");
	}
}
