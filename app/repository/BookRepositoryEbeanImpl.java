package repository;


import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.Transaction;
import models.Book;
import play.Logger;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 *
 * */
@Singleton
public class BookRepositoryEbeanImpl implements BookRepository
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private final EbeanServer ebeanServer;
	private final DatabaseExecutionContext executionContext;
	
	@Inject
	public BookRepositoryEbeanImpl(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext)
	{
		this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
		this.executionContext = executionContext;
	}
	
	@Override
	public CompletionStage<Stream<Book>> list()
	{
		return supplyAsync(() ->
		{
			return ebeanServer.find(Book.class).findList().stream();
			
		}, executionContext);
	}
	
	/**
	 * получаем книгу по идентификатору
	 * <p>
	 * @param id
	 * @return
	 */
	@Override
	public CompletionStage<Optional<Book>> get(String id)
	{
		return supplyAsync(() ->
		{
			return Optional.ofNullable(ebeanServer.find(Book.class).setId(id).findUnique());
		}, executionContext);
	}
	
	/**
	 * обновляем запись о книге по id
	 * <p>
	 * @param id
	 * @param dataOfNewBook
	 * @return
	 */
	@Override
	public CompletionStage<Optional<Book>> update(String id, Book dataOfNewBook)
	{
		return supplyAsync(() ->
		{
			Transaction txn = ebeanServer.beginTransaction();
			Optional<Book> value = Optional.empty();
			try
			{
				Book savedBook = ebeanServer.find(Book.class).setId(id).findUnique();
				if (savedBook != null)
				{
					//savedBook.name = dataOfNewBook.genreCollection;
					//savedBook.update();
					txn.commit();
					value = Optional.of(savedBook);
				}
			}
			finally
			{
				txn.end();
			}
			return value;
		}, executionContext);
	}
	
	/**
	 * удаляем запись по идентификатору
	 */
	@Override
	public CompletionStage<Optional<String>> delete(String id)
	{
		return supplyAsync(() ->
		{
			try
			{
				final Optional<Book> bookOptional = Optional.ofNullable(ebeanServer.find(Book.class).setId(id).findUnique());
				bookOptional.ifPresent(book -> book.delete());
				return bookOptional.map(book -> String.valueOf(book.id));
			}
			catch (Exception e)
			{
				logger.error("Не удалось удалить объект с идентификатором {}", id);
				return Optional.empty();
			}
		}, executionContext);
	}
	
	/**
	 * //добавляем запись
	 * @param book
	 * @return
	 */
	@Override
	public CompletionStage<Book> create(Book book)
	{
		return supplyAsync(() ->
		{
			ebeanServer.insert(book);
			return book;
		}, executionContext);
	}
}
