package repository;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.Transaction;
import models.Author;
import models.Book;
import play.Logger;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * */
@Singleton
public class AuthorRepositoryEbeanImpl implements AuthorRepository
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private final EbeanServer ebeanServer;
	private final DatabaseExecutionContext executionContext;
	
	@Inject
	public AuthorRepositoryEbeanImpl(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext)
	{
		this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
		this.executionContext = executionContext;
	}
	
	@Override
	public CompletionStage<Stream<Author>> list()
	{
		return supplyAsync(() ->
		{
			return ebeanServer.find(Author.class).findList().stream();
			
		}, executionContext);
	}
	
	@Override
	public CompletionStage<Author> create(Author author)
	{
		return supplyAsync(() ->
		{
			ebeanServer.insert(author);
			return author;
		}, executionContext);
	}
	
	/**
	 * получаем автора по идентификатору
	 * @param id UUID автора
	 * @return
	 */
	@Override
	public CompletionStage<Optional<Author>> get(String id)
	{
		return supplyAsync(() ->
		{
			return Optional.ofNullable(ebeanServer.find(Author.class).setId(id).findUnique());
		}, executionContext);
	}
	
	/**
	 * обновляем запись об авторе по id
	 * @param id идентификатор автора
	 * @param author объект автора с данными, которые мы хотим записать в БД
	 * @return Optional с объектом автора (обновленным)
	 * или без объекта автора, если не нашли автора с таким id
	 */
	@Override
	public CompletionStage<Optional<Author>> update(String id, Author author)
	{
		return supplyAsync(() ->
		{
			Transaction txn = ebeanServer.beginTransaction();
			Optional<Author> value = Optional.empty();
			try
			{
				Author savedAuthor = ebeanServer.find(Author.class).setId(id).findUnique();
				if (savedAuthor != null)
				{
					savedAuthor.setFirstname(author.getFirstname());
					savedAuthor.setMiddlename(author.getMiddlename());
					savedAuthor.setLastname(author.getLastname());
					
					savedAuthor.update();
					
					txn.commit();
					value = Optional.ofNullable(savedAuthor);
				}
			}
			catch (Exception ex)
			{
				logger.error("Не удалось обновить объект Author id = {}", id);
			}
			finally
			{
				txn.end();
			}
			return value;
		});
	}
	
	@Override
	public CompletionStage<Stream<Book>> loadAssociatedBookCollection(String id, int offset, int max)
	{
		return supplyAsync(() ->
		{
			Author author= ebeanServer.find(Author.class).setId(id).findUnique();
			
			if (author != null)
			{
				return ebeanServer.find(Book.class).where().eq("author", author)
						.orderBy("whenCreated")
						.setFirstRow(offset)
						.setMaxRows(max).findList().stream();
			}
			return Stream.empty();
			
		}, executionContext);
	}
	
	@Override
	public CompletionStage<Optional<String>> delete(String id)
	{
		return supplyAsync(() ->
		{
			try
			{
				final Optional<Author> authorOptional = Optional.ofNullable(ebeanServer.find(Author.class).setId(id).findUnique());
				authorOptional.ifPresent(c -> c.delete());
				return authorOptional.map(c -> String.valueOf(c.id));
			}
			catch (Exception e)
			{
				return Optional.empty();
			}
		}, executionContext);
	}
}
