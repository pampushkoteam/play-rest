package repository;

import models.Book;
import models.Genre;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 *
 */
public interface GenreRepository
{
	CompletionStage<Stream<Genre>> list();
	
	CompletionStage<Optional<String>> delete(String id);
	
	CompletionStage<Genre> create(Genre genre);
	
	CompletionStage<Optional<Genre>> get(String id);
	
	CompletionStage<Optional<Genre>> update(String id, Genre genre);
	
	CompletionStage <Stream<Book>> loadAssociatedBookCollection(String genreId, int offset, int max);
}
