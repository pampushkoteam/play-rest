package repository;

import models.Author;
import models.Book;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 *
 */
public interface AuthorRepository
{
	CompletionStage<Stream<Author>> list();
	
	CompletionStage<Author> create(Author author);
	
	CompletionStage<Optional<Author>> get(String id);
	
	CompletionStage<Optional<Author>> update(String id, Author author);
	
	CompletionStage<Optional<String>> delete(String id);
	
	CompletionStage<Stream<Book>> loadAssociatedBookCollection(String id, int offset, int max);
}
