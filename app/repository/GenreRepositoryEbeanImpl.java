package repository;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.Transaction;
import models.Book;
import models.Genre;
import play.Logger;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * */
@Singleton
public class GenreRepositoryEbeanImpl implements GenreRepository
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private final EbeanServer ebeanServer;
	private final DatabaseExecutionContext executionContext;
	
	@Inject
	public GenreRepositoryEbeanImpl(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext)
	{
		this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
		this.executionContext = executionContext;
	}
	
	@Override
	public CompletionStage<Stream<Genre>> list()
	{
		return supplyAsync(() ->
		{
			return ebeanServer.find(Genre.class).findList().stream();
			
		}, executionContext);
	}
	
	/**
	 * получаем запись о жанре по идентификатору
	 * @param id
	 * @return
	 */
	@Override
	public CompletionStage<Optional<Genre>> get(String id)
	{
		return supplyAsync(() ->
		{
			return Optional.ofNullable(ebeanServer.find(Genre.class).setId(id).findUnique());
		}, executionContext);
	}
	
	
	/**
	 * обновляем запись об жанре книги по id
	 * @param id
	 * @param genre
	 * @return
	 */
	@Override
	public CompletionStage<Optional<Genre>> update(String id, Genre genre)
	{
		return supplyAsync(() ->
		{
			Transaction txn = ebeanServer.beginTransaction();
			Optional<Genre> value = Optional.empty();
			try
			{
				Genre savedGenre = ebeanServer.find(Genre.class).setId(id).findUnique();
				if (savedGenre != null)
				{
					//savedGenre.state = genre.state;
					//savedGenre.name = genre.name;
					//savedGenre.update();
					txn.commit();
					value = Optional.ofNullable(savedGenre);
				}
			}
			finally
			{
				txn.end();
			}
			return value;
		}, executionContext);
	}
	
	/**
	 * получаем коллекцию книг по указанному идентификатору жанра книги
	 * @param genreId идентификатор жанра
	 * @param offset строка с которой начинаем вывод
	 * @param max максимальное количество строк в результате
	 * @return
	 */
	@Override
	public CompletionStage<Stream<Book>> loadAssociatedBookCollection(String genreId, int offset, int max)
	{
		return supplyAsync(() ->
		{
			Genre genre= ebeanServer.find(Genre.class).setId(genreId).findUnique();
			
			if (genre != null)
			{
				return ebeanServer.find(Book.class).where().in("genreCollection", genre)
						.orderBy("whenCreated")
						.setFirstRow(offset)
						.setMaxRows(max).findList().stream();
			}
			return Stream.empty();
			
		}, executionContext);
	}
	
	/**
	 * удаляем запись по идентификатору
	 */
	@Override
	public CompletionStage<Optional<String>> delete(String id)
	{
		return supplyAsync(() ->
		{
			try
			{
				final Optional<Genre> genreOptional = Optional.ofNullable(ebeanServer.find(Genre.class).setId(id).findUnique());
				genreOptional.ifPresent(c -> c.delete());
				return genreOptional.map(c -> String.valueOf(c.id));
			}
			catch (Exception e)
			{
				return Optional.empty();
			}
		}, executionContext);
	}
	
	/**
	 * добавляем запись
	 * @param genre
	 * @return
	 */
	@Override
	public CompletionStage<Genre> create(Genre genre)
	{
		return supplyAsync(() ->
		{
			Genre savedGenre = Genre.newBuilder().setName(genre.getName()).build();
			ebeanServer.insert(savedGenre);
			return savedGenre;
			
		}, executionContext);
	}
}
