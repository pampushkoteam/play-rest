package models;

import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 *     Book:
 * </p>
 * <p>
 *     У книги может быть только один автор (в реальности все сложнее, но для упрощения задачи пусть будет так).
 * </p>
 * <p>
 *     Книга может относится к нескольким жанрам.
 * </p>
 */
@Entity
@Table(name = "book")
public class Book extends BaseModel
{
	private static final long serialVersionUID = 4L;
	
	public Book()
	{
	
	}
	
	public Book(String name,
	            Author author,
	            Integer year,
	            Integer edition,
	            Set<Genre> genreCollection)
	{
		this.name = name;
		this.author = author;
		this.year = year;
		this.edition = edition;
		this.genreCollection = genreCollection;
	}
	
	@Constraints.Required
	public String name;
	
	@ManyToOne
	public Author author;
	
	//год публикации
	public Integer year;
	
	//номер издания книги
	public Integer edition;
	
	@ManyToMany(cascade = CascadeType.ALL)
	public Set<Genre> genreCollection = new HashSet<Genre>();
	
	
	public static Builder newBuilder()
	{
		return new Book().new Builder();
	}
	
	public class Builder
	{
		private Builder()
		{
			// private constructor
		}
		
		public Builder setName(String name)
		{
			Book.this.name = name;
			return this;
		}
		
		public Builder setAuthor(Author author)
		{
			Book.this.author = author;
			return this;
		}
		
		public Builder setYear(Integer year)
		{
			Book.this.year = year;
			return this;
		}
		
		public Builder setEdition(Integer edition)
		{
			Book.this.edition = edition;
			return this;
		}
		
		public Builder setGenreCollection(Set<Genre> genreCollection)
		{
			Book.this.genreCollection = genreCollection;
			return this;
		}
		
		public Book build()
		{
			return Book.this;
		}
	}
}
