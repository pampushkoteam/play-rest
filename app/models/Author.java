package models;

import play.Logger;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.lang.invoke.MethodHandles;
import java.util.Set;

/**
 * <p>
 *     Author:
 * </p>
 * <p>
 *     Автор может написать несколько книг.
 * </p>
 */
@Entity
@Table(name = "author")
public class Author extends BaseModel
{
	@Transient
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private static final long serialVersionUID = 1L;
	
	@Constraints.Required
	public String lastname;
	
	@Constraints.Required
	public String firstname;
	
	public String middlename;
	
	
	public Set<Book> bookCollection;
	
	
	public String getLastname()
	{
		return lastname;
	}
	
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}
	
	public String getFirstname()
	{
		return firstname;
	}
	
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}
	
	public String getMiddlename()
	{
		return middlename;
	}
	
	public void setMiddlename(String middlename)
	{
		this.middlename = middlename;
	}
	
	public Set<Book> getBookCollection()
	{
		return bookCollection;
	}
	
	public void setBookCollection(Set<Book> bookCollection)
	{
		this.bookCollection = bookCollection;
	}
	
	public static Author.Builder newBuilder()
	{
		return new Author().new Builder();
	}
	
	public class Builder
	{
		private Builder()
		{
			// private constructor
		}
		
		public Author.Builder setLastname(String lastname)
		{
			Author.this.lastname = lastname;
			return this;
		}
		
		public Author.Builder setFirstname(String firstname)
		{
			Author.this.firstname = firstname;
			return this;
		}
		
		public Author.Builder setMiddlename(String middlename)
		{
			Author.this.middlename = middlename;
			return this;
		}
		
		public Author.Builder setBookCollection(Set<Book> bookCollection)
		{
			Author.this.bookCollection = bookCollection;
			return this;
		}
		
		public Author build()
		{
			return Author.this;
		}
	}
	
}
