package models;

import play.data.validation.Constraints;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>Genre:</p>
 * <p>Жанр может относиться к нескольким книгам.</p>
 */
@Entity
@Table(name = "genre")
public class Genre extends BaseModel
{
	private static final long serialVersionUID = 2456L;

	public Genre()
	{
	
	}
	
	@Constraints.Required
	public String name;
	
	@ManyToMany(mappedBy = "genreCollection", cascade = CascadeType.ALL)
	public Set<Book> bookCollection = new HashSet<>();
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public Set<Book> getBookCollection()
	{
		return bookCollection;
	}
	
	public void setBookCollection(Set<Book> bookCollection)
	{
		this.bookCollection = bookCollection;
	}
	
	
	public static Genre.Builder newBuilder()
	{
		return new Genre().new Builder();
	}
	
	public class Builder
	{
		private Builder()
		{
			// private constructor
		}
		
		public Genre.Builder setName(String name)
		{
			Genre.this.name = name;
			return this;

		}
		
		public Genre.Builder setBookCollection(Set<Book> bookCollection)
		{
			Genre.this.bookCollection = bookCollection;
			return this;
		}
		
		public Genre build()
		{
			return Genre.this;
		}
	}
}
