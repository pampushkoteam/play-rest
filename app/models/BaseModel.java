package models;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@MappedSuperclass
public class BaseModel extends Model
{
	@Column(nullable = false,
			updatable = false,
			unique = true)
	@Id @GeneratedValue
	public UUID id;
	
	//счетчик кол-ва изменений записи
	@Version
	@Column(nullable = false)
	public Integer version;
	
	//дата+время создания записи
	@Column
	@CreatedTimestamp
	public LocalDateTime whenCreated;
	
	//дата+время последнего изменения записи
	@Column
	@UpdatedTimestamp
	public LocalDateTime whenUpdated;
	
	//состояние записи
	public State state = State.ACTIVE;
	
	@Enumerated(EnumType.STRING)
	public State getState()
	{
		return state;
	}
	
	public void setState(State state)
	{
		this.state = state;
	}
}


