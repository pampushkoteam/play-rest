package models;

/**
 * Состояние записи объекта
 */
public enum State
{
	ACTIVE,
	DELETED
}
