package models;

import play.data.validation.Constraints;

import javax.persistence.Entity;

//todo удалить
/**
 * Company entity managed by Ebean
 */
@Entity
public class Company extends BaseModelOldExamples
{
	private static final long serialVersionUID = 1L;
	
	@Constraints.Required
	public String name;
	
}

