package v1.books;

import v1.genres.GenreResourceIn;
import play.Logger;
import v1.authors.AuthorResourceIn;

import java.lang.invoke.MethodHandles;
import java.util.Set;

/**
 * Resource for the API.  This is a presentation class for frontend work.
 */
public class BookResourceIn
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	public BookResourceIn()
	{
	
	}
	
	//------------------------------------------------------------------------
	public String name;
	public AuthorResourceIn author;
	public Integer year;
	public Integer edition;
	public Set<GenreResourceIn> genreCollection;
	
	//------------------------------------------------------------------------
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public AuthorResourceIn getAuthor()
	{
		return author;
	}
	
	public void setAuthor(AuthorResourceIn author)
	{
		this.author = author;
	}
	
	public Integer getYear()
	{
		return year;
	}
	
	public void setYear(Integer year)
	{
		this.year = year;
	}
	
	public Integer getEdition()
	{
		return edition;
	}
	
	public void setEdition(Integer edition)
	{
		this.edition = edition;
	}
	
	public Set<GenreResourceIn> getGenreCollection()
	{
		return genreCollection;
	}
	
	public void setGenreCollection(Set<GenreResourceIn> genreCollection)
	{
		this.genreCollection = genreCollection;
	}
}

