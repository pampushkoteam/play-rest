package v1.books;

import models.Book;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import repository.BookRepository;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 *
 */
public class BookResourceHandler
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private final BookRepository repository;
	private final HttpExecutionContext httpExecutionContext;
	private final BookResourceHelper helper;
	
	@Inject
	public BookResourceHandler(BookRepository repository, HttpExecutionContext httpExecutionContext, BookResourceHelper helper)
	{
		this.helper = helper;
		this.repository = repository;
		this.httpExecutionContext = httpExecutionContext;
	}
	
	public CompletionStage<Stream<BookResourceOut>> find()
	{
		return repository.list().thenApplyAsync(bookDataStream ->
		{
			return bookDataStream.map(data ->
			{
				return new BookResourceOut(data);
			});
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<BookResourceOut> create(BookResourceIn resource)
	{
		final Book data = Book.newBuilder()
				.setName(resource.getName())
				.setAuthor(helper.convertAuthor(resource.getAuthor()))
				.setEdition(resource.getEdition())
				.setYear(resource.getYear())
				.setGenreCollection(helper.convertGenreCollection(resource.getGenreCollection()))
				.build();
		
		return repository.create(data).thenApplyAsync(savedData ->
		{
			return new BookResourceOut(savedData);
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Optional<BookResourceOut>> lookup(String id)
	{
		return repository.get(id).thenApplyAsync(optionalData ->
		{
			return optionalData.map(data -> new BookResourceOut(data));
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Optional<BookResourceOut>> update(String id, BookResourceIn resource)
	{
		final Book book = new Book();
		return repository.update(id, book).thenApplyAsync(optionalData ->
		{
			return optionalData.map(data -> new BookResourceOut(data));
		}, httpExecutionContext.current());
	}
	
}
