package v1.books;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import repository.BookRepository;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 *
 */
@With(BookAction.class)
public class BookController extends Controller
{
	private final play.Logger.ALogger logger = play.Logger.of(MethodHandles.lookup().lookupClass());
	
	private final HttpExecutionContext httpExecutionContext;
	
	private BookResourceHandler handler;
	
	private final BookRepository bookRepository;
	
	@Inject
	public BookController(BookRepository bookRepository,
	                      HttpExecutionContext httpExecutionContext,
	                      BookResourceHandler handler)
	{
		this.bookRepository = bookRepository;
		this.httpExecutionContext = httpExecutionContext;
		this.handler = handler;
	}
	
	/**
	 * Вернуть список книг
	 */
	public CompletionStage<Result> list()
	{
		return handler.find().thenApplyAsync(books ->
		{
			final List<BookResourceOut> bookList = books.collect(Collectors.toList());
			return ok(Json.toJson(bookList));
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Result> create()
	{
		JsonNode json = request().body().asJson();
		final BookResourceIn resource = Json.fromJson(json, BookResourceIn.class);
		return handler.create(resource).thenApplyAsync(savedResource ->
		{
			return created(Json.toJson(savedResource));
		}, httpExecutionContext.current());
	}
}
