package v1.books;

import com.google.inject.Inject;
import v1.genres.GenreResourceIn;
import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.Author;
import models.Genre;
import play.Logger;
import repository.AuthorRepository;
import repository.BookRepository;
import v1.authors.AuthorResourceIn;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */

public class BookResourceHelper
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	final private BookRepository bookRepository;
	final private AuthorRepository authorRepository;
	final private EbeanServer ebeanServer;
	
	@Inject
	public BookResourceHelper(BookRepository bookRepository,
	                          AuthorRepository authorRepository)
	{
		this.bookRepository = bookRepository;
		this.authorRepository = authorRepository;
		this.ebeanServer = Ebean.getDefaultServer();
	}
	
	public Author convertAuthor(AuthorResourceIn authorResourceIn)
	{
		Author author = null;
		String id = authorResourceIn.getId();
		if (!id.isEmpty())
		{
			author = ebeanServer.find(Author.class).setId(id).findUnique();
		}
		return author;
	}
	
	public Set<Genre> convertGenreCollection(Set<GenreResourceIn> genreResourceIn)
	{
		HashSet<Genre> result = new HashSet<Genre>();
		genreResourceIn.forEach(elem ->
		{
			String id = elem.getId();
			if (!id.isEmpty())
			{
				Genre genre = ebeanServer.find(Genre.class).setId(id).findUnique();
				if (genre != null)
				{
					result.add(genre);
				}
			}
		});
		return result;
	}
}
