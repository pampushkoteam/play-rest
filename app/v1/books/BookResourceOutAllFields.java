package v1.books;

import models.Book;
import play.Logger;
import v1.authors.AuthorResourceOut;
import v1.genres.GenreResourceOut;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class BookResourceOutAllFields extends BookResourceOut
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	public BookResourceOutAllFields(Book book)
	{
		super(book);
		
		this.name = book.name;
		//this.author = book.author;
		this.year = book.year;
		this.edition = book.edition;
		//this.genreCollection = book.genreCollection;
	}
	
	public String name;
	
	public AuthorResourceOut author;
	
	public Integer year;
	
	public Integer edition;
	
	public Set<GenreResourceOut> genreCollection = new HashSet<GenreResourceOut>();
}

