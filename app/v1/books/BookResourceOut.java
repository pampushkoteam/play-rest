package v1.books;

import models.Book;
import play.Logger;

import java.lang.invoke.MethodHandles;
import java.util.UUID;

/**
 *
 */
public class BookResourceOut
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	public BookResourceOut(Book book)
	{
		this.id = book.id;
		this.whenCreated = String.valueOf(book.whenCreated);
	}
	
	public UUID id;
	public String whenCreated;
	
	public UUID getId()
	{
		return id;
	}
	
	public void setId(UUID id)
	{
		this.id = id;
	}
	
	public String getWhenCreated()
	{
		return whenCreated;
	}
	
	public void setWhenCreated(String whenCreated)
	{
		this.whenCreated = whenCreated;
	}
}

