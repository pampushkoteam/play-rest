package v1.authors;

import models.Author;

import java.util.UUID;

/**
 *
 */
public class AuthorResourceOut
{
	public AuthorResourceOut()
	{
	
	}
	
	public AuthorResourceOut(Author author)
	{
		this.id = author.id;
		this.firstName = author.firstname;
		this.middleName = author.middlename;
		this.lastName = author.lastname;
		this.whenCreated = String.valueOf(author.whenCreated);
		this.whenUpdated = String.valueOf(author.whenUpdated);
		this.version = author.version;
	}
	
	public UUID id;
	public String firstName;
	public String middleName;
	public String lastName;
	public String whenCreated;
	public String whenUpdated;
	public Integer version;
}
