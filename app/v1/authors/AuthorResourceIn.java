package v1.authors;

import models.Book;

import java.util.Set;

/**
 *
 */
public class AuthorResourceIn
{
	public AuthorResourceIn()
	{
	
	}
	
	public String id;
	
	public String firstName;
	
	public String middleName;
	
	public String lastName;
	
	public Set<Book> bookCollection;
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getFirstname()
	{
		return firstName;
	}
	
	public void setFirstname(String firstname)
	{
		this.firstName = firstname;
	}
	
	public String getMiddleName()
	{
		return middleName;
	}
	
	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public Set<Book> getBookCollection()
	{
		return bookCollection;
	}
	
	public void setBookCollection(Set<Book> bookCollection)
	{
		this.bookCollection = bookCollection;
	}
}

