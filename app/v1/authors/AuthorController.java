package v1.authors;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import repository.AuthorRepository;
import v1.books.BookResourceOut;
import v1.genres.GenreResourceOut;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 *
 */
@With(AuthorAction.class)
public class AuthorController extends Controller
{
	private final play.Logger.ALogger logger = play.Logger.of(MethodHandles.lookup().lookupClass());
	
	private final HttpExecutionContext httpExecutionContext;
	
	private AuthorResourceHandler handler;
	
	private final AuthorRepository authorRepository;
	
	@Inject
	public AuthorController(AuthorRepository authorRepository,
	                      HttpExecutionContext httpExecutionContext,
	                      AuthorResourceHandler handler)
	{
		this.authorRepository = authorRepository;
		this.httpExecutionContext = httpExecutionContext;
		this.handler = handler;
	}
	
	/**
	 * Вернуть список книг определенного автора
	 *
	 * @param id идентификатор автора
	 * @param offset номер строки с которой начинать выводить результат
	 * @param max максимальное количество строк, которое вы хотите получить
	 * @return
	 */
	public CompletionStage<Result> loadAssociatedBookCollection(UUID id, int offset, int max)
	{
		return handler.loadAssociatedBookCollection(String.valueOf(id), offset, max).thenApplyAsync(books ->
		{
			List<BookResourceOut> bookList = books.collect(Collectors.toList());
			return ok(Json.toJson(bookList));
		});
	}
	
	/**
	 * Вернуть список авторов
	 */
	public CompletionStage<Result> list()
	{
		return handler.find().thenApplyAsync(authors ->
		{
			final List<GenreResourceOut> authorList = authors.collect(Collectors.toList());
			return ok(Json.toJson(authorList));
		}, httpExecutionContext.current());
	}
	
	/**
	 * Создать автора
	 * @return
	 */
	public CompletionStage<Result> create()
	{
		JsonNode json = request().body().asJson();
		final AuthorResourceIn resource = Json.fromJson(json, AuthorResourceIn.class);
		return handler.create(resource).thenApplyAsync(savedResource ->
		{
			return created(Json.toJson(savedResource));
		}, httpExecutionContext.current());
	}
	
	/**
	 * Обновить запись автора
	 *
	 */
	public CompletionStage<Result> update(UUID id)
	{
		JsonNode json = request().body().asJson();
		AuthorResourceIn resource = Json.fromJson(json, AuthorResourceIn.class);
		return handler.update(String.valueOf(id), resource).thenApplyAsync(optionalResource ->
		{
			return optionalResource.map(r ->
			ok(Json.toJson(r))).orElseGet(
					() -> notFound()
			);
		}, httpExecutionContext.current());
	}
}
