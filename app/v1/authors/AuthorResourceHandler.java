package v1.authors;

import models.Author;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import repository.AuthorRepository;
import v1.books.BookResourceHelper;
import v1.books.BookResourceOut;
import v1.genres.GenreResourceOut;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 *
 */
public class AuthorResourceHandler
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private final AuthorRepository repository;
	private final HttpExecutionContext httpExecutionContext;
	private final BookResourceHelper helper;
	
	@Inject
	public AuthorResourceHandler(AuthorRepository authorRepository, HttpExecutionContext httpExecutionContext, BookResourceHelper helper)
	{
		this.helper = helper;
		this.repository = authorRepository;
		this.httpExecutionContext = httpExecutionContext;
	}
	
	/**
	 * Выгрузить книги отобранные по жанру
	 * @return
	 */
	public CompletionStage<Stream<BookResourceOut>> loadAssociatedBookCollection(String id, int offset, int max)
	{
		return repository.loadAssociatedBookCollection(id, offset, max).thenApplyAsync(genreDataStream ->
		{
			return genreDataStream.map(data ->
			{
				return new BookResourceOut(data);
			});
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Stream<GenreResourceOut>> find()
	{
		return repository.list().thenApplyAsync(authorDataStream ->
		{
			return authorDataStream.map(data ->
			{
				return new GenreResourceOut();
			});
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<AuthorResourceOut> create(AuthorResourceIn resource)
	{
		final Author data = Author.newBuilder()
				.setFirstname(resource.getFirstname())
				.setMiddlename(resource.getMiddleName())
				.setLastname(resource.getLastName())
				.build();
		
		return repository.create(data).thenApplyAsync(savedData ->
		{
			return new AuthorResourceOut(savedData);
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Optional<AuthorResourceOut>> lookup(String id)
	{
		return repository.get(id).thenApplyAsync(optionalData ->
		{
			return optionalData.map(data -> new AuthorResourceOut(data));
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Optional<AuthorResourceOut>> update(String id, AuthorResourceIn resource)
	{
		final Author author = Author.newBuilder()
				.setFirstname(resource.getFirstname())
				.setMiddlename(resource.getMiddleName())
				.setLastname(resource.getLastName())
				.setBookCollection(resource.getBookCollection())
				.build();
		return repository.update(id, author).thenApplyAsync(optionalData ->
		{
			return optionalData.map(data -> new AuthorResourceOut(data));
		}, httpExecutionContext.current());
	}
}
