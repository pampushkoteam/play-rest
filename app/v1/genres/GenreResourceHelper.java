package v1.genres;

import com.google.inject.Inject;
import io.ebean.Ebean;
import io.ebean.EbeanServer;
import play.Logger;
import repository.AuthorRepository;
import repository.BookRepository;

import java.lang.invoke.MethodHandles;

/**
 *
 */

public class GenreResourceHelper
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	final private BookRepository bookRepository;
	final private AuthorRepository authorRepository;
	final private EbeanServer ebeanServer;
	
	@Inject
	public GenreResourceHelper(BookRepository bookRepository,
	                           AuthorRepository authorRepository)
	{
		this.bookRepository = bookRepository;
		this.authorRepository = authorRepository;
		this.ebeanServer = Ebean.getDefaultServer();
	}
}
