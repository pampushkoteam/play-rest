package v1.genres;

import models.Genre;

import java.util.UUID;

/**
 *
 */
public class GenreResourceOut
{
	public GenreResourceOut()
	{
	
	}
	public GenreResourceOut(Genre genre)
	{
		this.id = genre.id;
		this.name = genre.name;
	}
	
	public UUID id;
	public String name;
}
