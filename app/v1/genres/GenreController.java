package v1.genres;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import repository.BookRepository;
import v1.books.BookResourceOut;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 */
@With(GenreAction.class)
public class GenreController extends Controller
{
	private final play.Logger.ALogger logger = play.Logger.of(MethodHandles.lookup().lookupClass());
	
	private final HttpExecutionContext httpExecutionContext;
	
	private GenreResourceHandler handler;
	
	@Inject
	public GenreController(BookRepository bookRepository,
	                       HttpExecutionContext httpExecutionContext,
	                       GenreResourceHandler handler)
	{
		this.httpExecutionContext = httpExecutionContext;
		this.handler = handler;
	}
	
	/**
	 * Вывести список книг отфильтрованных по определенному жанру
	 * @param offset
	 * @param max
	 * @return
	 */
	public CompletionStage<Result> loadAssociatedBookCollection(UUID id, int offset, int max)
	{
		return handler.loadAssociatedBookCollection(String.valueOf(id), offset, max).thenApplyAsync(books ->
		{
			List<BookResourceOut> bookList = books.collect(Collectors.toList());
			return ok(Json.toJson(bookList));
		});
	}
	
	/**
	 * Показать список жанров
	 */
	public CompletionStage<Result> list()
	{
		return handler.find().thenApplyAsync(books ->
		{
			final List<GenreResourceOut> bookList = books.collect(Collectors.toList());
			return ok(Json.toJson(bookList));
		}, httpExecutionContext.current());
	}
	
	/**
	 * Создать жанр
	 * @return
	 */
	public CompletionStage<Result> create()
	{
		JsonNode json = request().body().asJson();
		final GenreResourceIn resource = Json.fromJson(json, GenreResourceIn.class);
		return handler.create(resource).thenApplyAsync(savedResource ->
		{
			return created(Json.toJson(savedResource));
		}, httpExecutionContext.current());
	}
}
