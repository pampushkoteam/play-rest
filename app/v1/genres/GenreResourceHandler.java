package v1.genres;

import models.Book;
import models.Genre;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import repository.GenreRepository;
import v1.books.BookResourceOut;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 *
 */
public class GenreResourceHandler
{
	private final Logger.ALogger logger = Logger.of(MethodHandles.lookup().lookupClass());
	
	private final GenreRepository repository;
	private final HttpExecutionContext httpExecutionContext;
	private final GenreResourceHelper helper;
	
	@Inject
	public GenreResourceHandler(GenreRepository repository, HttpExecutionContext httpExecutionContext, GenreResourceHelper helper)
	{
		this.helper = helper;
		this.repository = repository;
		this.httpExecutionContext = httpExecutionContext;
	}
	
	/**
	 * Выгрузить книги отобранные по жанру
	 * @return
	 */
	public CompletionStage<Stream<BookResourceOut>> loadAssociatedBookCollection(String id, int offset, int max)
	{
		return repository.loadAssociatedBookCollection(id, offset, max).thenApplyAsync(genreDataStream ->
		{
			return genreDataStream.map(data ->
			{
				return new BookResourceOut(data);
			});
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Stream<GenreResourceOut>> find()
	{
		return repository.list().thenApplyAsync(genreDataStream ->
		{
			return genreDataStream.map(data ->
			{
				return new GenreResourceOut(data);
			});
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<GenreResourceOut> create(GenreResourceIn resource)
	{
		final Genre data = Genre.newBuilder()
				.setName(resource.getName())
				.setBookCollection(new HashSet<Book>())
				.build();
		
		return repository.create(data).thenApplyAsync(savedData ->
		{
			return new GenreResourceOut(savedData);
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Optional<GenreResourceOut>> lookup(String id)
	{
		return repository.get(id).thenApplyAsync(optionalData ->
		{
			return optionalData.map(data -> new GenreResourceOut(data));
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Optional<GenreResourceOut>> update(String id, GenreResourceIn resource)
	{
		final Genre data = new Genre();
		return repository.update(id, data).thenApplyAsync(optionalData ->
		{
			return optionalData.map(op -> new GenreResourceOut(data));
		}, httpExecutionContext.current());
	}
	
}
